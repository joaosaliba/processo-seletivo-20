# Processo Seletivo - 20

Ferramentas utilizadas:
* Eclipse IDE
* TomCat v9.0
* MySql v8.0.23
* Insomnia ( Requisições API)

Com projeto  já Clonado e  ter aguardado o Maven ter feito download dos pacotes

Fazer instalação TomCat e Mysql, fazer configuração Eclipse de modo que o mesmo utilize servidor TomCat.

Necessário que pelo Terminal se desative o TomCat para que Eclipse possa fazer o controle do mesmo, o comando utilizado foi:
```  sudo systemctl stop tomcat ```
 
Importar projeo para Eclipse.

Vá ao eclipse  na tab Server, vá em propriedades do server  e alterar location para o server do Tomcat

Com MySql instalado na maquina,  vá ao arquivo persistence.xml e altere o usuário e a senha para a  definida na instalação.

Após isto vá ao terminal de comando crei um Schema para a aplicação poder user 
Primeiramente rodando o comando ```sudo mysql -p ```
Com isso será requerido senha do sistema e após senha do banco para que assim possa criar o Schema com o comando ```CREATE SCHEMA funcionarios; ```
Com isso é necessário ir ao Eclipse e dar RUN no projeto lembrando de selecionar o Servidor como o TomCat configurado

Para acessar 
``` localhost:8080/funcionarios```