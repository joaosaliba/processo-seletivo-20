var inicio = new Vue({
	el: "#inicio",
	data: {
		listaFuncionarios: [],
		funcionario: '',
		listaSetores:[],

	},
	created: function () {
		let vm = this;
		vm.buscaFuncionarios();
		vm.buscaSetores();
	},
	methods: {
		buscaFuncionarios: function () {
			const vm = this;
			axios.get("/funcionarios/rs/funcionarios")
				.then(response => {
					vm.listaFuncionarios = response.data;
				}).catch(function (error) {
					vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
				}).finally(function () {
				});
		},
		deleteFuncionario: function (id) {
			const vm = this;
			axios.delete("/funcionarios/rs/funcionarios/delete/" + id)
				.then(response => {


				}).finally(function () {
					vm.buscaFuncionarios();
				})
		},

		updateFuncionario: function (id) {
			const vm = this;
			axios.put("/funcionarios/rs/funcionarios/update/" + id, vm.funcionario)
				.then(response => {
					
					
					alert("Funcionario modificado com Sucesso");
				}).catch(function (eror){
					alert("Funcionario não modificado ");
					
				}).finally(function () {
					vm.buscaFuncionarios();
				})
		},

		pegaFuncionario : function(funcionarioEscolhido){
			const vm = this;
			vm.funcionario =  funcionarioEscolhido;

		},

		buscaSetores: function(){
			const vm = this;
			axios.get("/funcionarios/rs/setor/list")
			.then(response => {vm.listaSetores = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			})
		},

		
	}
});