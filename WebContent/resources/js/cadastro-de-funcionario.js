var inicio = new Vue({
	el:"#cadastro",
    data: {
    
		listaSetores:[],
		
		funcionario : {
			nome:'',
			setor:'',
			salario:'',
			email:'',
			idade:''
                
              },
			  
			  
    },
    created: function(){
        let vm =  this;
		vm.buscaFuncionarios();
		vm.buscaSetores();
		
    },
    methods:{
        buscaFuncionarios: function(){
			const vm = this;
			axios.get("/funcionarios/rs/funcionarios")
			.then(response => {vm.listaFuncionarios = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			})
		},

		createFuncionario: function (){
			const vm = this;
            axios.post("/funcionarios/rs/funcionarios/add",  vm.funcionario)
                .then(response => {
              	
                })
			},

			buscaSetores: function(){
				const vm = this;
				axios.get("/funcionarios/rs/setor/list")
				.then(response => {vm.listaSetores = response.data;
				}).catch(function (error) {
					vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
				}).finally(function() {
				})
			},
			
		  

		}
});